<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<c:import url="/WEB-INF/jsp/header.jsp" />
		<title>${msg}</title>
	</head>
	<body>
		<div class="vertical-center">
			<div class="container">
				<div class="row justify-content-center">
					<h1>Bienvenue !</h1>
				</div><br>
				<div class="row justify-content-center">
					<div class="col-md-7">
						<p class="center">
							Voici le TP Kotlin que tout le monde attendez ! (C'est le cas hein ?) <br>
							Pour vous aider au cours de ces exercices fabuleux, trois coll&egrave;gues sont la pour vous donner un coup de main.<br>
							Bien &eacute;videmment vous savez comment s'appellent ces trois personnes.<br> Mais on va quand m&ecirc;me vous donner leurs noms au cas
							ou vous auriez un trou de m&eacute;moire.
						</p>
					</div>
				</div><br>
				<div class="row justify-content-center">
					<div class="col-md-8">
						<ul>
							<li>Thomas (Le mec grand, dark, avec une barbe)</li>
							<li>Nicolas (Il a des lunettes)</li>
							<li>Valentin (Le dernier, tu devrais &ecirc;tre capable de deviner qui c'est)</li>
						</ul>
					</div>
				</div><br>
				<div class="row justify-content-center">
					<div class="col-md-8">
						<p class="center">
							Maintenant on vous laisse commencer avec le premier exercice. Tout est dans le Readme.<br>
							On vous laisse à disposition ce Front pour tester.
						</p>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>