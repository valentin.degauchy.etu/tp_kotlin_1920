<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<c:import url="/WEB-INF/jsp/header.jsp" />
		<title>Etape 2</title>
	</head>
	<body>
		<h1 class="content-title">Etape 2 : Gestion de Liste</h1>
		<br>
		<h3 class="content-sub-title">Saisie d'un nouveau Meme : </h3>
		<br><br>
		<div class="container-fluid">
			<form:form method="POST" action="/meme/add" modelAttribute="formMeme">
				<div class="row">
					<div class="col-sm-2 offset-md-4">
						Meme name: 
					</div>
					<div class="col-sm-auto">
						<form:input type="text" path="nom" placeholder="Nom"/>
					</div>
				</div><br>
				<div class="row">
					<div class="col-sm-2 offset-md-4">
						URL img: 
					</div>
					<div class="col-sm-auto">
						<form:input type="text" path="url" placeholder="URL de l'image"/>
					</div>
				</div><br>
				<div class="row">
					<div class="col-sm-4 offset-md-4 center">
						<input type="submit" value="Submit"/>
					</div>
				</div>
			</form:form><br>
			<c:choose>
				<c:when test="${not empty succeed}">
					<div class="row">
						<div class="col-sm-12">
							<h2 class="success center">
								Meme ajouté avec succ&eacute;s !
							</h2>
						</div>
					</div>
				</c:when>
			</c:choose>
		</div>	
	</body>
</html>